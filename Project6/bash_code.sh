########PROJECT 6###############

##Download SRA raw reads
wget -c https://sra-downloadb.be-md.ncbi.nlm.nih.gov/sos3/sra-pub-run-19/SRR12134661/SRR12134661.1


##Number of reads
INF1=raw_reads_R1.fastq.gz
INF2=raw_reads_R2.fastq.gz
gunzip -c $INF1 | grep "^@" | wc -l
#2648372
gunzip -c $INF2 | grep "^@" | wc -l
#2658606

#Length distributions
zcat $INF1 | awk '{if(NR%4==2) print length($1)}' | \
sort | uniq -c
zcat $INF2 | awk ’{if(NR%4==2) print length($1)}’ | \
sort | uniq -c

##Download reference genomes
esearch -db nucleotide -query NC_057567 | efetch -format fasta > Ref1.txt
esearch -db nucleotide -query NC_035680 | efetch -format fasta > Ref2.txt
cat Ref1.txt Ref2.txt > Refgenomes.fasta



##Defining input

# Defining input files
INF1=raw_reads_R1.fastq.gz
INF2=raw_reads_R2.fastq.gz
REFGENOMES=Refgenomes.fasta

# Building a database index for the reference genomes
mkdir -p db
bowtie2-build $REFGENOMES db/myRef > refdb.log

# Mapping reads with bowtie2 to reference genomes and
bowtie2 -x db/myRef -1 $INF1 -2 $INF2 2> mapping.err | \
# immediately converting the output to a binary file
samtools view -bS - > mapping.bam

# Inferring mapping statistics
samtools flagstat mapping.bam > mapping_stats.txt


# Extracting reads that are properly paired and fully mapped and
samtools view -b -F12 mapping.bam | \
samtools sort -n - > extracted_mappedF12.bam
bedtools bamtofastq -i extracted_mappedF12.bam -fq mappedF12_reads.fastq


awk -v n=4 'BEGIN{f=2} {if((NR-1)%n==0){f=1-f}; \
print > "mappedF12_reads_R" f ".fastq"}' mappedF12_reads.fastq
# Correcting a small formatting issue in file name
mv mappedF12_reads_R-1.fastq mappedF12_reads_R1.fastq


##Number of mapped pairs
grep "with itself and mate mapped" mapping_stats.txt | \
awk '{print $1}'

##Number of read in output files

grep "^@" mappedF12_reads_R*.fastq | uniq | wc -l

### Filtering reads mapped to the reverse strand
samtools view -b -F12 -F16 mapping.bam | \
samtools sort -n - > extracted_mappedF12F16.bam
bedtools bamtofastq -i extracted_mappedF12F16.bam \
-fq mappedF12F16_reads.fastq

awk -v n=4 'BEGIN{f=2} {if((NR-1)%n==0){f=1-f}; \
print > "mappedF12F16_reads_R" f ".fastq"}' \
mappedF12F16_reads.fastq

mv mappedF12F16_reads_R-1.fastq mappedF12F16_reads_R1.fastq
## This number is approx 50% of the mapped reads which is what would occur by chance.

grep "^@" mappedF12F16_reads_R*.fastq | uniq | wc -l


# Depth of sequencing coverage
# First we need to map reads to reference 
# genome via Bowtie2

INF1=mappedF12_reads_R1_reduced.fastq
INF2=mappedF12_reads_R2_reduced.fastq
REFGENOMES=Refgenomes.fasta

# Unfortunatelly in our reference genomes file
# there are '\n' signs which have to be deleted

# Deleting '\n'
cat Refgenomes.fasta | tr -d '\n' > Refgenomes_deinterleaved.fasta

# Next step is to manually change the file in such way
# that each genome starts in a new line as well as its header

# Manually changing the file
nano Refgenomes.fasta

# Extracting only the first reference genome as reference
head -n2 Refgenomes_deinterleaved > firstRef.fasta


mkdir -p db
bowtie2-build firstRef.fasta db/firstRef > firstRefdb.log

bowtie2 -x db/firstRef -1 $INF1 -2 $INF2 2> preAdj_mapping.err | \
	samtools view -bS - > preAdj_mapping.bam

# Visualizing coverage depth via bamcov

samtools sort preAdj_mapping.bam > preAdj_mapping.bam.sorted
samtools index preAdj_mapping.bam.sorted

touch preAdj_covHistogr.txt;
for i in ‘seq 0 1000 150000‘; do
	start=$(($i+1)); end=$(($i+1000));
	echo -n "${start}-${end} " >> preAdj_covHistogr.txt
	{YOUR_PATH}/bamcov/bamcov preAdj_mapping.bam.sorted -H \
	-r NC_057567.1:${start}-${end} | \
	awk ’{print $7}’ >> preAdj_covHistogr.txt;
done

# Display the visuals in the console

while read d n; do
	nw=$(echo "$n/50" | bc)
	printf "%s\t%${nw}s\n" "$d" = | tr ’ ’ ’=’
done < preAdj_covHistogr.txt

# Next we reduce the sequencing coverage by Kmer-based
# sequencing depth normalization
# Our ReadLenght is adjusted to our reads

ReadDepth=500
ReadLength=301
BestKmerSize=31

var1=$(echo "$ReadLength-$BestKmerSize+1" | bc)
var2=$(echo "scale=2;$ReadLength/$var1" | bc)
var3=$(echo "scale=2;$ReadDepth/$var2" | bc)
KmerDepth=${var3%.*}

# Normalizing sequencing depth

INF1=mappedF12_reads_R1_reduced.fastq
INF2=mappedF12_reads_R2_reduced.fastq
OTF1=plastomeReads_CovDepNormedAt${ReadDepth}_R1.fastq
OTF2=plastomeReads_CovDepNormedAt${ReadDepth}_R2.fastq
bbnorm.sh in=$INF1 in2=$INF2 out=$OTF1 out2=$OTF2 min=0 \
	target=$KmerDepth > CovDepNormedAt${ReadDepth}.log 2>&1

# Next we map the reads to reference genome via Bowtie2
# but using the output of BBNorm as input

INF1=plastomeReads_CovDepNormedAt${ReadDepth}_R1.fastq
INF2=plastomeReads_CovDepNormedAt${ReadDepth}_R2.fastq
bowtie2 -x db/firstRef -1 $INF1 -2 $INF2 2> postAdj_mapping.err | \
	samtools view -bS - > postAdj_mapping.bam

# Visualizing adjusted coverage depth of plastid genome

samtools sort postAdj_mapping.bam > postAdj_mapping.bam.sorted
samtools index postAdj_mapping.bam.sorted
touch postAdj_covHistogr.txt;
	for i in ‘seq 0 1000 150000‘; do
	start=$(($i+1)); end=$(($i+1000));
	echo -n "${start}-${end} " >> postAdj_covHistogr.txt
	/{YOUR_PATH}/bamcov/bamcov postAdj_mapping.bam.sorted -H \
	-r Ref1:${start}-${end} | \
	awk ’{print $7}’ >> postAdj_covHistogr.txt;
done

while read d n; do
	nw=$(echo "$n/50" | bc)
	printf "%s\t%${nw}s\n" "$d" = | tr ’ ’ ’=’
done < postAdj_covHistogr.txt


##Plasid genome assembly with NOVOPlasty of not normalized sequences

echo ">mySeedSequence" > mySeedSequence.fasta

head -n2 mappedF12_reads_R1_reduced.fastq | \
tail -n1 >> mySeedSequence.fasta

## specify the correct file in the NOVOPlasty config file, 301 length
nano NOVOPlasty/config.txt 

perl NOVOPlasty/NOVOPlasty4.3.1.pl -c NOVOPlasty/config.txt
## best config file is 1 with 56405 bp
## we have 5 contigs

##with normalisation
head -n2 plastomeReads_CovDepNormedAt500_R1.fastq | tail -n1 >> mySeedSequence.fasta

## specify the correct file in the NOVOPlasty config file, 301 length
nano NOVOPlasty/config.txt 

perl NOVOPlasty/NOVOPlasty4.3.1.pl -c NOVOPlasty/config.txt
## best config file is 1 with 56405 bp
## we have 3 contigs
