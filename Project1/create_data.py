import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import requests
from sklearn.model_selection import KFold

def load_dataset(save=False):
  """
  This fuction is meant to be exported so this is why here download part repeats itself.
  """
  url = "https://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/processed.cleveland.data"
  r = requests.get(url, allow_redirects=True)
  open('processed.cleveland.data', 'wb').write(r.content)

  df = pd.read_csv("processed.cleveland.data", header=None, na_values="?", names=["age", "sex", "cp", "trestbps", "chol", "fbs", "restecg", "thalach", "exang", "oldpeak", "slope", "ca", "thal", "num"])
  df['num'].values[df['num'].values > 0] = 1
  df.dropna(inplace=True)
  if save:
    df.to_csv("cleveland.csv",index=False)

  return df

if __name__ == "__main__":
    load_dataset(True)