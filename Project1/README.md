# PROJECT 1 README

We have here 3 files:

* `create_data.py`: Downloads and creates dataset.
* `preprocesing.ipynb`: Data check.
* `create_models.R`: Models building. 

## How to run it?

### Install all requirements

For python there is `requirements.txt` file defined.
R script will download and install all necessary libraries.

### Run

Run python `create_data.py` script to create data.
Run R `create_models.R` script to check models output.
