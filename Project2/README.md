# PROJECT 2 README

We have here 4 files:

* `create_data.py`: Downloads and creates dataset.
* `p2_preprocesing.ipynb`: Data check.
* `random_forest_classifier_project2.py` : Building the random forest model (also in .ipynb)
* `svm_project_02.R`: Models building. 

## How to run it?

### Install all requirements

For python there is `requirements.txt` file defined.
R script will download and install all necessary libraries.

### Run

Run the bash script `run.sh` that will execute the python and R scripts.
