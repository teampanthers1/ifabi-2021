import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import requests

def load_dataset(save=False):
  """
  This fuction is meant to be exported so this is why here download part repeats itself.
  """
  url = "https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data"
  r = requests.get(url, allow_redirects=True)
  open('wdbc.data', 'wb').write(r.content)

  names = ['diagnosis', 'radius_mean', 'texture_mean', 'perimeter_mean',
       'area_mean', 'smoothness_mean', 'compactness_mean', 'concavity_mean',
       'concave_points_mean', 'symmetry_mean', 'fractal_dimension_mean',
       'radius_se', 'texture_se', 'perimeter_se', 'area_se', 'smoothness_se',
       'compactness_se', 'concavity_se', 'concave points_se', 'symmetry_se',
       'fractal_dimension_se', 'radius_worst', 'texture_worst',
       'perimeter_worst', 'area_worst', 'smoothness_worst',
       'compactness_worst', 'concavity_worst', 'concave_points_worst',
       'symmetry_worst', 'fractal_dimension_worst']

  df = pd.read_csv("wdbc.data", header=None, na_values="?", names=names)
  # df['diagnosis'].values[df['diagnosis'].values == "M"] = 1
  # df['diagnosis'].values[df['diagnosis'].values == "B"] = 0
  # df['diagnosis'] = df['diagnosis'].astype('category')

  # df.dropna(inplace=True)
  if save:
    df.to_csv("wisconsin.csv",index=False)

  return df

if __name__ == "__main__":
    load_dataset(True)