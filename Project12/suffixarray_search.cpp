#include <divsufsort.h>
#include <sstream>

#include <seqan3/std/filesystem>

#include <seqan3/alphabet/nucleotide/dna5.hpp>
#include <seqan3/argument_parser/all.hpp>
#include <seqan3/core/debug_stream.hpp>
#include <seqan3/io/sequence_file/all.hpp>
#include <seqan3/search/fm_index/fm_index.hpp>
#include <seqan3/search/search.hpp>
using namespace std;

int Binarysearch(std::vector<seqan3::dna5>const& reference,
  std::vector<int> const& suffixarray, std::vector<seqan3::dna5>const& q,
  bool is_last){
    //Function that using binary search returns the first or the last occurence
    // of the query (depending on is_last)
    int result = -1;
    int L = 0;
    int R = suffixarray.size() - 1;

    while (L <= R){
      int m = (L+R)/2, qindex = 0, qsize = q.size();
      auto ref_i = suffixarray.at(m);
      char char_at_m, char_at_q;

      while (qindex < qsize){
        char_at_q = seqan3::to_char(q.at(qindex));
        char_at_m = seqan3::to_char(reference.at(ref_i+qindex));
        if (char_at_q != char_at_m) break;
        qindex++;
      }

      if (char_at_m == char_at_q){
          result = m;
          if (!is_last) R = m-1; // we want to search for the first occurence so go to the left
          else L = m + 1; // we want to search for the last occurence so go to the right
      }
      else if (char_at_m > char_at_q){
        R = m -1 ;
      } else{
        L = m + 1;
      }
    }

    return result;
}

int main(int argc, char const* const* argv) {
    seqan3::argument_parser parser{"suffixarray_search", argc, argv, seqan3::update_notifications::off};

    parser.info.author = "SeqAn-Team";
    parser.info.version = "1.0.0";

    auto reference_file = std::filesystem::path{};
    parser.add_option(reference_file, '\0', "reference", "path to the reference file");

    auto query_file = std::filesystem::path{};
    parser.add_option(query_file, '\0', "query", "path to the query file");

    try {
         parser.parse();
    } catch (seqan3::argument_parser_error const& ext) {
        seqan3::debug_stream << "Parsing error. " << ext.what() << "\n";
        return EXIT_FAILURE;
    }

    // loading our files
    auto reference_stream = seqan3::sequence_file_input{reference_file};
    auto query_stream     = seqan3::sequence_file_input{query_file};

    // read reference into memory
    // Attention: we are concatenating all sequences into one big combined sequence
    //            this is done to simplify the implementation of suffix_arrays
    std::vector<seqan3::dna5> reference;
    for (auto& record : reference_stream) {
        auto r = record.sequence();
        reference.insert(reference.end(), r.begin(), r.end());
    }

    // read query into memory
    std::vector<std::vector<seqan3::dna5>> queries;
    for (auto& record : query_stream) {
        queries.push_back(record.sequence());
    }

    //ALEK THIS IS YOUR PART
    //!TODO here adjust the number of searches : Alek here is where you change
    // the number of queries !!!!! ex 4) the file you should use  as input (in command line)
    //is illumina_reads_100.fasta.gz

    // For exercise number 5 you should use the files in /data in the repository
    // 40, 60, 80, 100
    //and  leave queries.resize(100);
    queries.resize(100); //will reduce the number of queries

    // Array that should hold the future suffix array
    std::vector<saidx_t> suffixarray;
    int n = reference.size();
    suffixarray.resize(n) ;
    saidx_t* ptr = suffixarray.data();
    sauchar_t const* str = reinterpret_cast<sauchar_t const*>(reference.data());

    divsufsort(str, ptr, (saidx_t) n); // this function creates the suffix array

    auto start = high_resolution_clock::now();
    //Find the number of occurences for each query
    for(auto& q : queries){
      int firstoccurence = Binarysearch(reference, suffixarray, q, false);
      int lastoccurrence = Binarysearch(reference, suffixarray, q, true);
      if (firstoccurence == -1){
        cout << "The querie was found 0 times" << endl;
      }
      else{
        cout << "The querie was found "
        << lastoccurrence - firstoccurence + 1 << " times" << endl;
      }

      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<microseconds>(stop - start);
      cout << duration.count() << endl;
    }
    return 0;
}
