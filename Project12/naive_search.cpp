#include <sstream>

#include <seqan3/std/filesystem>

#include <seqan3/alphabet/nucleotide/dna5.hpp>
#include <seqan3/argument_parser/all.hpp>
#include <seqan3/core/debug_stream.hpp>
#include <seqan3/io/sequence_file/all.hpp>
#include <seqan3/search/fm_index/fm_index.hpp>
#include <chrono>
#include <seqan3/search/search.hpp>
using namespace std;
using namespace std::chrono;

// prints out all occurences of query inside of ref
void findOccurences(std::vector<seqan3::dna5> const& ref, std::vector<seqan3::dna5> const& query) {
  int M = query.size();
  int N = ref.size();
  int counter = 0;
  for (int i = 0; i <= N - M; i++) {
      int j;

      for (j = 0; j < M; j++) {
          if (ref[i + j] != query[j])
              break;
      }
      if (j == M){
          //cout << "Query found at index " << i << endl;
          counter += 1 ;
      }
  }
  cout << "Found " << counter << " occurences" << endl;
}

int main(int argc, char const* const* argv) {
    seqan3::argument_parser parser{"naive_search", argc, argv, seqan3::update_notifications::off};

    parser.info.author = "SeqAn-Team";
    parser.info.version = "1.0.0";

    auto reference_file = std::filesystem::path{};
    parser.add_option(reference_file, '\0', "reference", "path to the reference file");

    auto query_file = std::filesystem::path{};
    parser.add_option(query_file, '\0', "query", "path to the query file");

    try {
         parser.parse();
    } catch (seqan3::argument_parser_error const& ext) {
        seqan3::debug_stream << "Parsing error. " << ext.what() << "\n";
        return EXIT_FAILURE;
    }


    // loading our files
    auto reference_stream = seqan3::sequence_file_input{reference_file};
    auto query_stream     = seqan3::sequence_file_input{query_file};

    // read reference into memory
    std::vector<std::vector<seqan3::dna5>> reference;
    for (auto& record : reference_stream) {
        reference.push_back(record.sequence());
    }

    // read query into memory
    std::vector<std::vector<seqan3::dna5>> queries;
    for (auto& record : query_stream) {
        queries.push_back(record.sequence());
    }

    //!TODO !CHANGEME here adjust the number of searches

    //!TODO here adjust the number of searches : Alek here is where you change
    // the number of queries !!!!! ex 4) the file you should use  as input (in command line)
    //is illumina_reads_100.fasta.gz

    // For exercise number 5 you should use the files in /data in the repository
    // 40, 60, 80, 100
    //and  leave queries.resize(100);
    queries.resize(10000); // will reduce the amount of searches

    auto start = high_resolution_clock::now();
    //! search for all occurences of queries inside of reference
    for (auto& r : reference) {
        for (auto& q : queries) {
            findOccurences(r, q);
        }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    cout << duration.count() << endl;
    return 0;
}
