# IFABI-2021

A repository for 2021/22 Introduction to Focus Area class for bioinformatics Master's at FU Berlin. Members: 

* Sara Lopez Ruiz de Vargas
* Inga Tummoszeit
* Aleksander Muck
* Jakub Otreba


# What is Git?

Git is the most commonly used version control system. Git tracks the changes you make to files, so you have a record of what has been done, and you can revert to specific versions should you ever need to. Git also makes collaboration easier, allowing changes by multiple people to all be merged into one source.

So regardless of whether you write code that only you will see, or work as part of a team, Git will be useful for you.

Nice git tutorial: https://www.atlassian.com/git/tutorials

# How do we work with Git and GitLab?

Our style should be based on COMMUNICATION AND UNDERSTANDING.

It is not about killing each other, but to facilitate and improve cooperation.

The starting point for our work is GitHub flow.

# Github flow

This flow is ideal for organizations that need simplicity, and roll out frequently. If you are already using Git, you are probably using a version of the Github flow. Every unit of work, whether it be a bugfix or feature, is done through a branch that is created from master. After the work has been completed in the branch, it is reviewed and tested before being merged into master and pushed out to production.

https://files.programster.org/tutorials/git/flows/github-flow.png

As it is simple flow all branches are taken from master and, in some point, will be merged back to master.

Branches
In a nutshell, the branches we support:

* `master`: Current versions of the production code.
* `feature/feature_name` : Place to work on new features.
* `fix/fix_name`: Patching bugs from master.

# Master branch

Communication with the `master` takes place using a `merge requests` with fast forwarding, and commits will be squashed.

No one has rights to push something directly to `master` and that option is disabled by default, but if during our work this will be annoying then we will resume option to push directly to `master`.

# Feature branches

Each task should have at least one feature branch. Such branches must be rebased to `master` as often as possible to simplify future merging.

Rebasing is important to keep new commits together for possible tuning or squashing in the feature.

Whenever a feature is ready (or a stable part of the feature) it can be merged with the `master` branch.

Feature branches should be deleted as soon as their commits are merged into the `master`.

# Fix branches

It is a branch for fixes. It should contain only fixes for the released versions, which can't wait until the next scheduled release of the product.

Once a hot fix is merged back into the `master` the branch should be deleted as soon as merged with the `master`.

# Merge (or pull) requests

Merge Request and Pull request basically refers to same thing. Tools such as GitHub and Bitbucket choose the name pull request since the first manual action would be to pull the feature branch.

Tools such as GitLab and Gitorious choose the name merge request since that is the final action that is requested of the assignee.

Pull/Merge requests are created if you are working in a own branch and wants to merge your change into main branch(eg. `master`).

The merge requests serves as a code review tool and if your code reveals shortcomings/issues anyone can commit and push a fix.

# Merge Conflicts

When you're combining two branches with `git merge` or `git rebase`, git usually doesn't complain unless the branches have conflicting versions of one or more files. It's important to note that "conflicting" doesn't just mean "different," it means "can't be combined without losing changes." In general, you get merge conflicts after two people have been editing the same line(s) in the same file concurrently.

Merge conflicts are common enough that a number of tools have been developed just to resolve them.

When conflict occurs person that is responsible for branch lifecycle should resolve all conflicts.

Work of many people on one branch
It will often happen that more than one person will work on one branch. In such a case, one person responsible for a given branch should be selected.

He will be responsible for the correct merging of changes, describing them and considering possible conflicts in the code and taking care of the correct flow in general.

# How does code review work?

Code review is one of the programming practices that consist an analysis of the code by another programmer, called a reviewer. This action takes place before merging branch into master.

To understand what we need code review for and what it is for, read the following articles:

* https://www.atlassian.com/agile/software-development/code-reviews
* https://www.brightspot.com/products/developer-life-5-reasons-why-the-code-review-process-is-critical-for-developers

After reading them, we should know:

* Why is code review important,
* Why do I want to do code review,
* Why do I want someone to review my own code.
* Such a solution will also increase the independence of programmers, teach us many new things and significantly increase the quality of the code.

However, it should be remembered that code review is not the PURPOSE of the work!

We should not spend more than several dozen minutes on the procedure itself, unless the changes are really big. The review shouldn't take longer than the development itself, so let's not go into the deepest details!

# Docstrings

Docstrings will be crated only when function is more complex and doctrig can really improve quality of the code.

Docstrings will contain only basic info about the function\class definition.

Please keep in mind that all function, classes and parameters should me meaningful!

During code review reviewer should also check whenever docstring would be helpful.


# Flow example

Here I want to show you small example of our flow.

## Point zero

For example, let's say that as a developer, you were given the task of adding support for the new version of pandas and improving the operation on dataframes.

You know very well that such a change will require the work of two developers. It was you who was chosen as the responsible person for a given branch and introducing the new functionality!

Congratulations! :D

## Gather requirements

Your first step should be to collect the requirements and you should know what are you doing.

## Create branch

At this point, it is necessary to create a new branch with a name that matches the scheme: `feature/name_of_feature`.

In this case, the appropriate name is e.g. `feature/pandas_update`.

The source of your branch should be branch `master`.

## Create merge request

The next step is to create a new merge request based on our new branch. To do this, select the Merge Requests field in the menu on the left.

After selecting the window, a list of merge requests will appear. We are interested in the green button New merge request.

A window will appear in which you need to choose the source branch and target branch. In our case, source to `feature/pandas_update` and the target will be `master`.

* The name of the merge request should coincide with the name of the branch.
* The name should be preceded by the prefix WIP (it means that we are still working on it at the moment). Gitlab will automatically block the merge requests marked as WIP.
* Create a description that will tell more exactly what the job is about. The task should have its counterpart in the trello card or GitLab Issue. One of the predefined templates can be used for this.
Remember to assign a responsible person (yourself), complete the description and select the appropriate labels!

## Development

At this point, you can start developing and working on the code together with other developers.

Other people without asking should not comment on the WIP code.

Sometimes someone needs help\comment in referring to a given place in the code. To do this, simply select our merge request in the merge request tab and click on the Changes tab:

After you hover over the place we are looking for, a window with a dialogue bubble will appear:

After clicking on them, a window will appear where we can start a discussion \ leave a comment or call a person to a chosen place in the code and ask what you want or communicate something:

An important element of the work should be writing the documentation. Leaving code comments and docstrings behind can be very important in the future.

If necessary, documentation should be modified or added (the documentation topic will be developed further in the document).


## The end of development

Take off the WIP status (remove the prefix from the name) and ask another developer for a code review.

This is done by marking the other person as Assignee for the task.

Code review should be done on GitLab. The procedure is similar to the one presented for leaving comments inside GitLab, which was described in the previous section.

In doubtful areas, a discussion (not a comment) should be created and resolved in one way or another.

## Merging

At this point, if all steps in the checklist have been marked as completed, you can proceed to the merging procedure.

If the responsible person himself is able to resolve conflicts, he can do so himself.

If not contact authors of unresolvable parts of the feature.

When there is no conflict ask Kamran for approval. When received mark merge request as approved and click MERGE button!

And all is done! :D
