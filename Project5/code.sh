
esearch -db sra -query "genome AND (plastid OR chloroplast)" | \
efetch -format native | \
xtract -pattern SAMPLE -element SCIENTIFIC_NAME \
TAXON_ID LABEL PRIMARY_ID EXTERNAL_ID > mySRA_IDs.txt

inFile=mySRA_IDs.txt
rm -f xd.txt
touch xd.txt
while read -r line; do
    taxon_name=$(echo $line | awk '{print $1" "$2}')
    SAMN_num=$(echo $line | awk '{print $(NF-2)}')
    echo -n "$taxon_name $SAMN_num " >> xd.txt;
    esearch -db biosample -query "$SAMN_num" </dev/null | \
    elink -target nucleotide  | \
    xtract -pattern ENTREZ_DIRECT -element Count>> xd.txt;
done < ${inFile};

MYQUERY="complete genome[TITLE]\
AND (chloroplast[TITLE] OR plastid[TITLE]) \
AND 2020/01/01:2020/12/31[PDAT] \
AND 50000:250000[SLEN] \
NOT unverified[TITLE] NOT partial[TITLE]"

esearch -db nucleotide -query "$MYQUERY" | \
efetch -format acc | \
sort > myTestData_UIDs.txt

rm -f myTestData_Dates.txt
touch myTestData_Dates.txt
for acc in $(cat myTestData_UIDs.txt); do
echo -n "$acc " >> myTestData_Dates.txt;
esearch -db nucleotide -query "$acc" | \
efetch -format docsum | \
xtract -pattern DocumentSummary -element CreateDate | \
tr '/' '-' >> myTestData_Dates.txt;
done
