#Visualisation Sara Lopez Ruiz de Vargas, Aleksander Muck,
#Jakub Otreba and Inga Tummoszeit

library(zoo)

#read data 
my_data <- read.delim(file.choose(),header = FALSE,sep = ' ')#myTestData_Dates-1.txt
colnames(my_data) <- c('records','date')

length(unique(my_data$records)) # 626 different records

#formatting the dates
my_data$date <- as.Date(my_data$date, format="%Y-%m-%d")
tableDates <- table(my_data$date)
barplot(tableDates)

days <- sort(my_data$date-as.Date("2020-01-01")) #date as days + sorted
tableDays <- table(days)             
#cumulative grow rate in 365 days n=ending days - beginning days 
# ((endingvalue/beginningvalue)^(1/n))-1
cagr <- ((cum[length(cum)]/cum[1])^(1/(364-as.numeric(names(tableDays)[1])))-1)

b <- barplot(cum,xlab='days since 2020-01-01',ylab='cumulative growth across 2020')
lines(x=as.numeric(b),y=cagr*c(as.numeric(names(tableDays)))*100,
      lwd=2,col='red')
axis(side=1, at=as.numeric(b),labels=names(tableDays))


#alternativ plot 
#cumulative sum for every 365 days
cum <- as.numeric(cumsum(tableDays))
cumDays <- data.frame(sum=rep(NA,365))
rownames(cumDays) <- c(1:365)
cumDays[1,1] <- 0
cumDays[as.numeric(names(tableDays)),] <- cum
cumDays$sum <- na.locf(cumDays$sum) 

b <- barplot(cumDays$sum,xlab='days',ylab='cumulative sum')
lines(x=as.numeric(b),y=cagr*c(1:365)*100,
      lwd=2,col='red')
axis(side=1, at=as.numeric(b),labels=1:365)
